BEHAT INSTALLATION FOR DEMO
===========================

install composer
--------

  $ curl -sS https://getcomposer.org/installer | php

Install the Vendors using composer
----------------------------------

  $ composer install

test behat installation by checking version
--------

  $ bin/behat -v


Lauch the selenium server:
--------------------------

  $ java -jar thirdPartyTools/selenium-server-standalone-2.41.0.jar

lauch the test suite:
--------------------

  $ bin/behat
