Feature: test mesmateriaux card 
  In order to command on the website 
  As a non-register customer
  I need to add/remove some products on the card


     
  @javascript
  Scenario: search a product on the main page  
     Given I am on "/"
     When I fill in "q" with "ciment"
     And I press "ok"
     Then I should see "Ciment gris 32,5R NF sac 25kg"
  
  @javascript   
  Scenario: visit a product page and add in card  
     Given I am on "/achat-materiaux/ciment-gris-32-5r-nf-sac-25kg_384.html"
     When I click on ajouterAuPanier 
     And I wait 25 seconds
     Then I should be on "achat-materiaux/panier.html"
     Then I should see "Ciment gris 32,5R NF sac 25kg"

  @javascript   
  Scenario: visit a another product page and add in card  
     Given I am on "achat-materiaux/parpaing-bloc-creux-nf-b40-20x20x50cm_1312.html"
     When I click on ajouterAuPanier 
     And I wait 25 seconds
     Then I should be on "achat-materiaux/panier.html"
     Then I should see "Parpaing bloc creux NF B40 20x20x50cm"

 # @javascript   
 # Scenario: change product qauntity in card  
 #    Given I am on "achat-materiaux/panier.html"
 #    Then I should see "Pour afficher vos tarifs, vous devez saisir votre localité de livraison." 
 #    When I fill in "geoloc-commande" with "Le Mans"
 #    And I wait 1 seconds
 #    And I click on css link ".LinkVille"
 #    When I click on css link ".PlusMoinsBtnP" 
 #    Then the "PanierNbr[ 384 ]" field should contain "2"





  