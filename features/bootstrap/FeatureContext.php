<?php

use Behat\Behat\Context\ClosuredContextInterface,
    Behat\Behat\Context\TranslatedContextInterface,
    Behat\Behat\Context\BehatContext,
    Behat\Behat\Exception\PendingException;
use Behat\Gherkin\Node\PyStringNode,
    Behat\Gherkin\Node\TableNode;

use Behat\MinkExtension\Context\MinkContext;

//
// Require 3rd-party libraries here:
//
//   require_once 'PHPUnit/Autoload.php';
//   require_once 'PHPUnit/Framework/Assert/Functions.php';
//

/**
 * Features context.
 */
class FeatureContext extends MinkContext
{
    /**
     * Initializes context.
     * Every scenario gets it's own context object.
     *
     * @param array $parameters context parameters (set them up through behat.yml)
     */
    public function __construct(array $parameters)
    {
        // Initialize your context here
    }

    /**
        * @Given /^I wait (\d+) seconds$/
        */
       public function iWaitSeconds($arg1)
       {
           $this->getSession()->wait($arg1*1000
                 
              );
              
       }
       /**
            * @Given /^I click on css link "([^"]*)"$/
            */
           public function iClickOnCssLink($arg1)
           {
               $session = $this->getSession(); // get the mink session
               $element = $session->getPage()->find(
                      'css',
                      $arg1
                  );
                 
                  if (null === $element) {
                      throw new \InvalidArgumentException(sprintf('Could not find button add to card'));
                  }
                  $element->click();
           }
               
       /**
           * @When /^I click on ajouterAuPanier$/
           */
          public function iClickOnAjouteraupanier()
          {
              $this->iClickOnCssLink('.big-button-default-h46-panier');
          }   
    /**
        * @When /^I follow with xPath "([^"]*)"$/
        */
       

//
// Place your definition and hook methods here:
//
//    /**
//     * @Given /^I have done something with "([^"]*)"$/
//     */
//    public function iHaveDoneSomethingWith($argument)
//    {
//        doSomethingWith($argument);
//    }
//
}
